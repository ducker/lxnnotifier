//
//  LXNNotifier.h
//  LXNNotifier
//
//  Created by Leszek Kaczor on 12/03/15.
//
//  Copyright (c) 2017 Leszek Kaczor
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


@import UIKit;

NS_ASSUME_NONNULL_BEGIN

@class LXNNotifier;

typedef NS_ENUM(NSUInteger, LXNNotifierPosition) {
    LXNNotifierPositionTop,
    LXNNotifierPositionMid,
    LXNNotifierPositionBottom,
    LXNNotifierPositionCount, //leave it at the end of enum
};

typedef void (^LXNNotifierCompletedAnimationBlock)();
typedef void (^LXNNotifierDismissBlock)();
typedef void (^LXNNotifierAnimationBlock)(UIView * view, LXNNotifierCompletedAnimationBlock completionBlock);

extern const CGFloat LXNNotifierInfiniteDuration;

@protocol LXNNotifierDelegate

/**
 *  If delegate method returns nil view from viewForDisplayingNotificationForPosition: will be used
 */
- (nullable UIView *)notifier:(LXNNotifier *)notifier viewForDisplayingNotificationForPosition:(LXNNotifierPosition)position;

@optional

- (void)notifier:(LXNNotifier *)notifier willShowNotification:(UIView *)view atPosition:(LXNNotifierPosition)position;
- (void)notifier:(LXNNotifier *)notifier didDismissedNotification:(UIView *)view atPosition:(LXNNotifierPosition)position;

@end


@interface LXNNotifier : NSObject

/**
 *  Default 2 seconds
 */
@property (nonatomic, assign) CGFloat notificationDuration;
@property (nonatomic, weak  ) id<LXNNotifierDelegate> delegate;

+ (instancetype)sharedInstance;
- (UIView *)showNotificationWithView:(UIView *)view;
- (UIView *)showNotificationWithView:(UIView *)view position:(LXNNotifierPosition)position;
- (UIView *)showNotificationWithView:(UIView *)view position:(LXNNotifierPosition)position shouldDismissOnTap:(BOOL)shouldDismiss dismissOnTapBlock:(nullable LXNNotifierDismissBlock)dismissBlock;
- (UIView *)showNotificationWithView:(UIView *)view position:(LXNNotifierPosition)position notificationDuration:(CGFloat)duration shouldDismissOnTap:(BOOL)shouldDismiss dismissOnTapBlock:(nullable LXNNotifierDismissBlock)dismissBlock;
- (void)hideNotificationWithView:(UIView *)view;

- (nullable UIView *)currentNotificationViewForPosition:(LXNNotifierPosition)position;

- (void)setShowAnimation:(LXNNotifierAnimationBlock)animation forPosition:(LXNNotifierPosition)position;
- (void)setHideAnimation:(LXNNotifierAnimationBlock)animation forPosition:(LXNNotifierPosition)position;

/**
 *  @return if view is dismissing and cannot increase duration returns NO, otherwise returns YES
 */
- (BOOL)increaseNotificationDurationBy:(CGFloat)value forView:(UIView *)view;

/**
 *  If view is presented set notification duration from current time. If not presented just set notification duration.
 *
 *  @param value a notification duration
 *  @param view  a view to display
 *
 *  @return if view is dismissing and cannot increase duration returns NO, otherwise returns YES
 */
- (BOOL)setNotificationDurationFromNow:(CGFloat)value forView:(UIView *)view;

/**
 *  Returns view on which notification will be displayed. 
 *  By default it returns application's key window.
 *  You can override if if you want to change place notifications are presenting.
 *  If delegate is defined view from delegate is used.
 *
 *  @param position an animation position for which view is defined to display
 *
 *  @return the view for displaying notification
 */
- (UIView *)viewForDisplayingNotificationForPosition:(LXNNotifierPosition)position;

- (NSUInteger)numberOfWaitingNotificationsForPosition:(LXNNotifierPosition)position;

@end

NS_ASSUME_NONNULL_END
